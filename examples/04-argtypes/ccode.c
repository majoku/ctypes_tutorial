#include <stdlib.h>
#include <stdio.h>

double my_function(int64_t i, double d, char *s)
{
    printf("i = %ld\n", i);
    printf("d = %f\n", d);
    printf("s = %s\n", s);

    return i + d;
}
