import numpy as np
import ctypes as ct
import os

# Load library
libname = 'libmylib'
libpath = os.path.join(os.path.dirname(__file__), '.')
mylib = np.ctypeslib.load_library(libname, libpath)

# Define interface
mylib.my_function.argtypes = [
        ct.c_int64,
        ct.c_double,
        ct.c_char_p
]
mylib.my_function.restype = ct.c_double

# Call function
i = 1
d = 2.0
s = 'Hello'
result = mylib.my_function(i, d, s)
print('result = %f' % result)
