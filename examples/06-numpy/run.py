import numpy as np
import ctypes as ct
import os

# Load library
libname = 'libmylib'
libpath = os.path.join(os.path.dirname(__file__), '.')
mylib = np.ctypeslib.load_library(libname, libpath)

# Define interface
mylib.my_function.argtypes = [
        ct.c_size_t,
        np.ctypeslib.ndpointer(ndim=1, dtype=np.double, flags='A,C')
]
mylib.my_function.restype = ct.c_int

# Wrapper
def my_function(a):
    ierr = mylib.my_function(len(a), a)
    if ierr: raise RuntimeError('Error in libmylib.my_function')

# Call wrapper
a = np.arange(6.0)
my_function(a)
