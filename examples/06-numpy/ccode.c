#include <stdlib.h>
#include <stdio.h>

int my_function(size_t n, double a[])
{
    if (!a) return 1;

    for (size_t i = 0; i < n; i++) {
        printf("a[%ld] = %f\n", i, a[i]);
    }

    return 0;
}
