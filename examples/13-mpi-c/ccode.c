#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

#include "mpi.h"

double my_sum(size_t size, double array[])
{
    assert(size >= 0 && array);

    /* Get MPI size and rank */
    int mpi_size, mpi_rank;
    MPI_Comm_size(MPI_COMM_WORLD, &mpi_size);
    MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank);

    /* Local sum */
    double sum = 0.0;
    for (size_t idx = mpi_rank; idx < size; idx+=mpi_size) {
        sum += array[idx];
    }

    /* Reduce to world sum */
    double world_sum = 0.0;
    MPI_Reduce(&sum, &world_sum, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
    return world_sum;
}
