import numpy as np
import ctypes as ct
import os
from mpi4py import MPI

# MPI
mpi_world = MPI.COMM_WORLD
mpi_rank = mpi_world.Get_rank()

# Load library
libname = 'libmylib'
libpath = os.path.join(os.path.dirname(__file__), '.')
mylib = np.ctypeslib.load_library(libname, libpath)

# Define interface
mylib.my_sum.argtypes = [
        ct.c_size_t,
        np.ctypeslib.ndpointer(dtype=np.double, flags='A,C')
]
mylib.my_sum.restype = ct.c_double

# Wrapper
def my_sum(a):
    n = len(a) if a is not None else 0
    return mylib.my_sum(n, a)

# Run and timeit
from timeit import default_timer
np.random.seed(0)
array = np.random.rand(2**20)

# my_sum
t0 = default_timer()
res_my_sum = my_sum(array)
time_my_sum = default_timer() - t0

# np.sum
t0 = default_timer()
res_np_sum = np.sum(array)
time_np_sum = default_timer() - t0

if mpi_rank == 0:
    if not np.allclose(res_my_sum, res_np_sum):
        raise RuntimeError()
    print('Time my_sum: %f s, time np.sum: %f s' % (time_my_sum, time_np_sum))
