module my_module
use, intrinsic :: iso_fortran_env
use :: iso_c_binding
implicit none

contains

subroutine my_subroutine(i, d, s) bind(C)

    integer(c_int64_t), intent(in) :: i
    real(c_double), intent(in) :: d
    character(c_char), dimension(*), intent(in) :: s

    integer :: s_len = 0
    do
        if (s(s_len+1) == c_null_char) exit
        s_len = s_len + 1
    end do

    write(*,*) "i = ", i
    write(*,*) "d = ", d
    write(*,*) "s = ", s(:s_len)

end subroutine

end module
