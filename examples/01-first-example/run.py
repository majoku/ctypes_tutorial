import numpy as np
import ctypes as ct
import os

# Load library
libname = 'libmylib'
libpath = os.path.join(os.path.dirname(__file__), '.')
mylib = np.ctypeslib.load_library(libname, libpath)

# Call function
i = 1
d = 2.0
s = 'Hello'
mylib.my_function(
        ct.c_int64(i),
        ct.c_double(d),
        ct.c_char_p(s))
