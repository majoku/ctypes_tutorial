module my_module
use, intrinsic :: iso_fortran_env
implicit none

contains

subroutine my_subroutine(n, a, ierr)

    integer(INT64), intent(in) :: n
    real(REAL64), dimension(n), intent(in) :: a
    integer(INT64), intent(out) :: ierr
    integer :: i

    do i = 1, n
        write(*, '(a,i1,a,f8.6)') "a[", i, "] = ", a(i)
    end do

    ierr = 0

end subroutine

end module
