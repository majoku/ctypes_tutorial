import numpy as np
import ctypes as ct
import os

# Load library
libname = 'libmylib'
libpath = os.path.join(os.path.dirname(__file__), '.')
mylib = np.ctypeslib.load_library(libname, libpath)

# Define interface
mylib.__my_module_MOD_my_subroutine.argtypes = [
        ct.POINTER(ct.c_int64),
        np.ctypeslib.ndpointer(ndim=1, dtype=np.double, flags='A,F'),
        ct.POINTER(ct.c_int64)
]
mylib.__my_module_MOD_my_subroutine.restype = None

# Wrapper
def some_function(a):
    ierr = ct.c_int64()
    mylib.__my_module_MOD_my_subroutine(
            ct.byref(ct.c_int64(len(a))),
            a,
            ct.byref(ierr))
    if ierr.value: raise RuntimeError()

# Call wrapper
a = np.arange(6.0)
some_function(a)
