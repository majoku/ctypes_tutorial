import numpy as np
import ctypes as ct
import os
from mpi4py import MPI

# MPI
mpi_world = MPI.COMM_WORLD
mpi_size = mpi_world.Get_size()
mpi_rank = mpi_world.Get_rank()

# Figure out MPI communicator type (depends on MPI Library)
if MPI._sizeof(MPI.Comm) == ct.sizeof(ct.c_int):
    MPI_Comm_t = ct.c_int
else:
    MPI_Comm_t = ct.c_void_p

# Load library
libname = 'libmylib'
libpath = os.path.join(os.path.dirname(__file__), '.')
mylib = np.ctypeslib.load_library(libname, libpath)

# Define interface
mylib.my_sum.argtypes = [
        MPI_Comm_t,
        ct.c_size_t,
        np.ctypeslib.ndpointer(dtype=np.double, flags='A,C')
]
mylib.my_sum.restype = ct.c_double

# Convert mpi4py communicator to MPI Communicator
def get_comm_handle(comm):
    comm_ptr = MPI._addressof(comm)
    comm_val = MPI_Comm_t.from_address(comm_ptr)
    return comm_val

# Wrapper
def my_sum(a, comm):
    n = len(a) if a is not None else 0
    handle = get_comm_handle(comm)
    return mylib.my_sum(handle, n, a)

# Run and timeit
from timeit import default_timer
np.random.seed(0)
array = np.random.rand(2**20)

# my_sum
t0 = default_timer()
local_sum = my_sum(array, mpi_world)
res_my_sum = mpi_world.reduce(local_sum, MPI.SUM, 0)
time_my_sum = default_timer() - t0

# np.sum
t0 = default_timer()
res_np_sum = np.sum(array)
time_np_sum = default_timer() - t0

if mpi_rank == 0:
    if not np.allclose(res_my_sum, res_np_sum):
        raise RuntimeError()
    print('Time my_sum: %f s, time np.sum: %f s' % (time_my_sum, time_np_sum))
