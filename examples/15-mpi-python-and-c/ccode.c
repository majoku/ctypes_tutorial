#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

#include "mpi.h"

double my_sum(MPI_Comm comm, size_t size, double array[])
{
    assert(size >= 0 && array);

    /* Get MPI size and rank */
    int mpi_size, mpi_rank;
    MPI_Comm_size(comm, &mpi_size);
    MPI_Comm_rank(comm, &mpi_rank);

    /* Calculate and return LOCAL sum */
    double sum = 0.0;
    for (size_t idx = mpi_rank; idx < size; idx += mpi_size) {
        sum += array[idx];
    }
    return sum;
}
