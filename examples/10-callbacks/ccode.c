#include <stdlib.h>
#include <stdio.h>

int my_function(size_t n, double x[], double (*func)(double))
{
    if (!func) return 1;

    for (size_t i = 0; i < n; i++) {
        printf("func(%f) = %f\n", x[i], func(x[i]));
    }

    return 0;
}
