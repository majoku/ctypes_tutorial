import numpy as np
import ctypes as ct
import os

# Load library
libname = 'libmylib'
libpath = os.path.join(os.path.dirname(__file__), '.')
mylib = np.ctypeslib.load_library(libname, libpath)

# Define C function type
cfunc_t = ct.CFUNCTYPE(ct.c_double, ct.c_double)

# Define interface
mylib.my_function.argtypes = [
        ct.c_size_t,
        np.ctypeslib.ndpointer(ndim=1, dtype=np.double, flags='A,C'),
        cfunc_t
]
mylib.my_function.restype = ct.c_int

# Wrapper
def my_function(x, func):
    cfunc = cfunc_t(func)
    ierr = mylib.my_function(len(x), x, cfunc)
    if ierr: raise RuntimeError('Error in libmylib.my_function')

# Call wrapper
x = np.arange(10.0)
f = lambda x : x**2.0
my_function(x, f)
