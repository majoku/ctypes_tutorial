import numpy as np
import ctypes as ct
import os

# Load library
libname = 'libmylib'
libpath = os.path.join(os.path.dirname(__file__), '.')
mylib = np.ctypeslib.load_library(libname, libpath)

# Define Allocator class
class Allocator(object):
    """Allocator class to callback from C to Python for memory allocation.

    Use:
        foo.argtypes = [..., Allocator.CFUNCTYPE,...]
        alloc = Allocator()
        foo(..., alloc.cfunc,...)
        result = alloc.value
    """

    CFUNCTYPE = ct.CFUNCTYPE(ct.c_void_p, ct.c_size_t, ct.POINTER(ct.c_size_t), ct.c_char)

    def __init__(self):
        self.value = None

    def __call__(self, ndim, shape, dtype):
        try:
            self.value = np.zeros(shape=shape[:ndim], dtype=np.dtype(dtype))
            return self.value.ctypes.data
        except Exception as e:
            print(e)
            return None

    @property
    def cfunc(self):
        return self.CFUNCTYPE(self)

# Define interface
mylib.my_function.argtypes = [
        Allocator.CFUNCTYPE
]
mylib.my_function.restype = ct.c_int

# Wrapper
def my_function():
    alloc = Allocator()
    ierr = mylib.my_function(alloc.cfunc)
    if ierr: raise RuntimeError('Error in libmylib.my_function')
    return alloc.value

# Call wrapper
array = my_function()
print(array)
