#include <stdlib.h>
#include <stdio.h>

typedef void* (*allocator_t)(size_t, size_t[], char);

int my_function(allocator_t alloc)
{
    /* Allocate a 2D double array with shape (3, 4) */
    size_t ndim = 2;
    size_t shape[] = {3, 4};
    char dtype = 'd';
    double *array = (double *) alloc(ndim, shape, dtype);

    /* Fill array with values */
    for (size_t i = 0; i < shape[0]; i++) {
        for (size_t j = 0; j < shape[1]; j++) {
            array[i*shape[1]+j] = i*shape[1]+j;
        }
    }

    return 0;
}
