module my_module
use, intrinsic :: iso_fortran_env
implicit none

contains

subroutine my_subroutine(i, d, s)

    integer(INT64), intent(in) :: i
    real(REAL64), intent(in) :: d
    character(len=*), intent(in) :: s

    write(*,*) "i = ", i
    write(*,*) "d = ", d
    write(*,*) "s = ", s

end subroutine

end module
