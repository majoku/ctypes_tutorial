import numpy as np
import ctypes as ct
import os

# Load library
libname = 'libmylib'
libpath = os.path.join(os.path.dirname(__file__), '.')
mylib = np.ctypeslib.load_library(libname, libpath)

# Call function
i = 1
d = 2.0
s = 'Hello'
mylib.__my_module_MOD_my_subroutine(
        ct.byref(ct.c_int64(i)),
        ct.byref(ct.c_double(d)),
        ct.c_char_p(s),
        ct.c_int(len(s)))
