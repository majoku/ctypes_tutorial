#include <stdlib.h>
#include <stdio.h>

struct my_array {
    size_t size;
    double *data;
};

int my_function(struct my_array *a)
{
    if (!a) return 1;
    if (!a->data) return 2;

    for (size_t i = 0; i < a->size; i++) {
        printf("a[%ld] = %f\n", i, a->data[i]);
    }

    return 0;
}
