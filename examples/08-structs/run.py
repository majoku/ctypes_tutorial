import numpy as np
import ctypes as ct
import os

# Load library
libname = 'libmylib'
libpath = os.path.join(os.path.dirname(__file__), '.')
mylib = np.ctypeslib.load_library(libname, libpath)

# Define my_array struct
class my_array(ct.Structure):
    _fields_ = (
            ('size', ct.c_size_t),
            ('data', np.ctypeslib.ndpointer(ndim=1, dtype=np.double, flags='A,C')))

# Define interface
mylib.my_function.argtypes = [
    ct.POINTER(my_array)
]
mylib.my_function.restype = ct.c_int

# Wrapper
def my_function(a):
    struct = my_array(len(a), a.ctypes.data)
    ierr = mylib.my_function(struct)
    if ierr: raise RuntimeError('Error in libmylib.my_function')

# Call wrapper
a = np.arange(6.0)
my_function(a)
