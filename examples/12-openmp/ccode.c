#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

double my_sum(size_t size, double array[])
{
    assert(size >= 0 && array);

    double sum = 0.0;
#ifdef _OPENMP
#pragma omp parallel for reduction(+:sum)
#endif
    for (size_t idx = 0; idx < size; idx++) {
        sum += array[idx];
    }
    return sum;
}
