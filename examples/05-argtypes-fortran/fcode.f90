module my_module
use, intrinsic :: iso_fortran_env
implicit none

contains

subroutine my_subroutine(i, d, s, r)

    integer(INT64), intent(in) :: i
    real(REAL64), intent(in) :: d
    character(len=*), intent(in) :: s
    real(REAL64), intent(out) :: r

    write(*,*) "i = ", i
    write(*,*) "d = ", d
    write(*,*) "s = ", s
    r = i + d

end subroutine

end module
