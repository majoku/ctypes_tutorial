import numpy as np
import ctypes as ct
import os

# Load library
libname = 'libmylib'
libpath = os.path.join(os.path.dirname(__file__), '.')
mylib = np.ctypeslib.load_library(libname, libpath)

# Define interface
mylib.__my_module_MOD_my_subroutine.argtypes = [
        ct.POINTER(ct.c_int64),
        ct.POINTER(ct.c_double),
        ct.c_char_p,
        ct.POINTER(ct.c_double),
        ct.c_int
]
mylib.__my_module_MOD_my_subroutine.restype = None

# Call function
i = 1
d = 2.0
s = 'Hello'
result = ct.c_double()
mylib.__my_module_MOD_my_subroutine(
        ct.byref(ct.c_int64(i)),
        ct.byref(ct.c_double(d)),
        s,
        ct.byref(result),
        len(s))
print('result = %f' % result.value)
