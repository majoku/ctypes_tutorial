import numpy as np
import ctypes as ct
import os

# Load library
libname = 'libmylib'
libpath = os.path.join(os.path.dirname(__file__), '.')
mylib = np.ctypeslib.load_library(libname, libpath)

# NumPy pointer that can also be None
def ndpointer_or_none(base):
    def from_param(cls, obj):
        if obj is None:
            return obj
        return base.from_param(obj)
    return type(base.__name__, (base,), {'from_param': classmethod(from_param)})

# Define interface
mylib.my_function.argtypes = [
        ct.c_size_t,
        np.ctypeslib.ndpointer(ndim=1, dtype=np.double, flags='A,C'),
        ndpointer_or_none(np.ctypeslib.ndpointer(ndim=1, dtype=np.double, flags='A,C'))
]
mylib.my_function.restype = ct.c_int

# Wrapper
def my_function(a, b=None):
    ierr = mylib.my_function(len(a), a, b)
    if ierr: raise RuntimeError('Error in libmylib.my_function')

# Call wrapper
a = np.arange(6.0)
my_function(a)
b = np.arange(6.0)
my_function(a, b)
