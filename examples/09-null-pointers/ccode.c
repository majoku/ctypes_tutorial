#include <stdlib.h>
#include <stdio.h>

int my_function(size_t n, double a[], double b[])
{
    if (!a) return 1;

    /* Argument b is not NULL */
    if (b) {
        for (size_t i = 0; i < n; i++) {
            printf("a[%ld]+b[%ld] = %f\n", i, i, a[i]+b[i]);
        }
    /* Argument b is NULL */
    } else {
        for (size_t i = 0; i < n; i++) {
            printf("a[%ld] = %f\n", i, a[i]);
        }
    }

    return 0;
}
