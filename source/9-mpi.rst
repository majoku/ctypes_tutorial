MPI
===

To use python together with the message passing protocol (MPI),
we need to import mpi4py (http://mpi4py.scipy.org/docs/).

.. code-block:: python
    :caption: Short example:

    from mpi4py import MPI

    mpi_world = MPI.COMM_WORLD
    mpi_rank = mpi_world.Get_rank()
    mpi_size = mpi_world.Get_size()

    print('This is rank %d out of %d' % (mpi_rank, mpi_size))

.. code-block:: none
    :caption: Output

    $ mpiexec -n 4 python run.py
    This is rank 3 out of 4
    This is rank 2 out of 4
    This is rank 0 out of 4
    This is rank 1 out of 4

We can distinguish three cases:
    1. MPI calls only in the Python code

    2. MPI calls only in the C code

    3. MPI calls only in both the Python and C code

The first case does not require any change in our C code our ctypes interfaces.

In the second case, we only use MPI calls (data scattering, reductions,...) in our C code,
but mpi4py is still needed to limit Input/Output to the master process (rank 0). It does, however, also automatically
calls ``MPI_Init`` for us and registers ``MPI_Finalize`` to be called when the Python interpreter
exits.

The third (general) case is the most involved, since we need
to pass the MPI communicator to our C code.

.. code-block:: C
    :caption: ccode.c

    #include <stdlib.h>
    #include <stdio.h>
    #include <assert.h>

    #include "mpi.h"

    double my_sum(MPI_Comm comm, size_t size, double array[])
    {
        assert(size >= 0 && array);

        /* Get MPI size and rank */
        int mpi_size, mpi_rank;
        MPI_Comm_size(comm, &mpi_size);
        MPI_Comm_rank(comm, &mpi_rank);

        /* Calculate and return LOCAL sum */
        double sum = 0.0;
        for (size_t idx = mpi_rank; idx < size; idx += mpi_size) {
            sum += array[idx];
        }
        return sum;
    }


The MPI standard does not define how a communicator should be implemented.
This requires some work around in the Python code:

.. code-block:: python
    :caption: run.py
    :emphasize-lines: 11-15, 24, 30-34, 39

    import numpy as np
    import ctypes as ct
    import os
    from mpi4py import MPI

    # MPI
    mpi_world = MPI.COMM_WORLD
    mpi_size = mpi_world.Get_size()
    mpi_rank = mpi_world.Get_rank()

    # Figure out MPI communicator type (depends on MPI Library)
    if MPI._sizeof(MPI.Comm) == ct.sizeof(ct.c_int):
        MPI_Comm_t = ct.c_int
    else:
        MPI_Comm_t = ct.c_void_p

    # Load library
    libname = 'libmylib'
    libpath = os.path.join(os.path.dirname(__file__), '.')
    mylib = np.ctypeslib.load_library(libname, libpath)

    # Define interface
    mylib.my_sum.argtypes = [
            MPI_Comm_t,
            ct.c_size_t,
            np.ctypeslib.ndpointer(dtype=np.double, flags='A,C')
    ]
    mylib.my_sum.restype = ct.c_double

    # Convert mpi4py communicator to MPI Communicator
    def get_comm_handle(comm):
        comm_ptr = MPI._addressof(comm)
        comm_val = MPI_Comm_t.from_address(comm_ptr)
        return comm_val

    # Wrapper
    def my_sum(a, comm):
        n = len(a) if a is not None else 0
        handle = get_comm_handle(comm)
        return mylib.my_sum(handle, n, a)

    # Run and timeit
    from timeit import default_timer
    np.random.seed(0)
    array = np.random.rand(2**20)

    # my_sum
    t0 = default_timer()
    local_sum = my_sum(array, mpi_world)
    res_my_sum = mpi_world.reduce(local_sum, MPI.SUM, 0)
    time_my_sum = default_timer() - t0

    # np.sum
    t0 = default_timer()
    res_np_sum = np.sum(array)
    time_np_sum = default_timer() - t0

    if mpi_rank == 0:
        if not np.allclose(res_my_sum, res_np_sum):
            raise RuntimeError()
        print('Time my_sum: %f s, time np.sum: %f s' % (time_my_sum, time_np_sum))

