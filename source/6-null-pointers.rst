Passing NULL to pointers
========================

C does not support optional parameters. However, it can be useful to pass ``NULL``
for pointer types to indicate that the argument is not needed.

Example:

.. code-block:: c
    :caption: ccode.c

    #include <stdlib.h>
    #include <stdio.h>

    int my_function(size_t n, double a[], double b[])
    {
        if (!a) return 1;

        /* Argument b is not NULL */
        if (b) {
            for (size_t i = 0; i < n; i++) {
                printf("a[%ld]+b[%ld] = %f\n", i, i, a[i]+b[i]);
            }
        /* Argument b is NULL */
        } else {
            for (size_t i = 0; i < n; i++) {
                printf("a[%ld] = %f\n", i, a[i]);
            }
        }

    return 0;
    }

Unfortunately, NumPy's ``ndpointer`` will always raise a ``TypeError`` when we try to pass ``None`` to the library function.

The following factory provides a work around (see also https://github.com/numpy/numpy/issues/6239):

.. code-block:: python
    :caption: run.py
    :emphasize-lines: 10-16, 22

    import numpy as np
    import ctypes as ct
    import os

    # Load library
    libname = 'libmylib'
    libpath = os.path.join(os.path.dirname(__file__), '.')
    mylib = np.ctypeslib.load_library(libname, libpath)

    # NumPy pointer that can also be None
    def ndpointer_or_none(base):
        def from_param(cls, obj):
            if obj is None:
                return obj
            return base.from_param(obj)
        return type(base.__name__, (base,), {'from_param': classmethod(from_param)})

    # Define interface
    mylib.my_function.argtypes = [
            ct.c_size_t,
            np.ctypeslib.ndpointer(ndim=1, dtype=np.double, flags='A,C'),
            ndpointer_or_none(np.ctypeslib.ndpointer(ndim=1, dtype=np.double, flags='A,C'))
    ]
    mylib.my_function.restype = ct.c_int

    # Wrapper
    def my_function(a, b=None):
        ierr = mylib.my_function(len(a), a, b)
        if ierr: raise RuntimeError('Error in libmylib.my_function')

    # Call wrapper
    a = np.arange(6.0)
    my_function(a)
    b = np.arange(6.0)
    my_function(a, b)

.. code-block:: none
	:caption: Output

	a[0] = 0.000000
	a[1] = 1.000000
	a[2] = 2.000000
	a[3] = 3.000000
	a[4] = 4.000000
	a[5] = 5.000000
	a[0]+b[0] = 0.000000
	a[1]+b[1] = 2.000000
	a[2]+b[2] = 4.000000
	a[3]+b[3] = 6.000000
	a[4]+b[4] = 8.000000
	a[5]+b[5] = 10.000000
