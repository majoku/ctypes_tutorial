Same example with Fortran
=========================

Two possibilities: use standardized C ABI (since Fortran 2003)
or rely on compiler dependent Fortran ABI.

Fortran ABI (GNU)
-----------------

Similar code to previous example in C (but in module and with Fortran style string):

.. code-block:: fortran
    :caption: fcode.f90

    module my_module
    use, intrinsic :: iso_fortran_env
    implicit none

    contains

    subroutine my_subroutine(i, d, s)

    integer(INT64), intent(in) :: i
    real(REAL64), intent(in) :: d
    character(len=*), intent(in) :: s

    write(*,*) "i = ", i
    write(*,*) "d = ", d
    write(*,*) "s = ", s

    end subroutine

    end module

.. code-block:: none
    :caption: Compiling and linking like before:

    gfortran -c -fPIC fcode.f90
    gfortran -shared -Wl,-soname,libmylib.so fcode.o -o libmylib.so

.. code-block:: none
    :emphasize-lines: 4
    :caption: Inspecting the symbol table shows that the subroutine name got mangled by gfortran:

    nm libmylib.so
    ...
                     w _ITM_registerTMCloneTable
    0000000000000830 T __my_module_MOD_my_subroutine
    0000000000000790 t register_tm_clones
    ...

How the symbols are mangled is compiler dependent, you have to check it yourself!

The library subroutine now needs to be called as:

.. code-block:: python
    :caption: run.py
    :emphasize-lines: 14-18

    import numpy as np
    import ctypes as ct
    import os

    # Load library
    libname = 'libmyname'
    libpath = os.path.join(os.path.dirname(__file__), '.')
    mylib = np.ctypeslib.load_library(libname, libpath)

    # Call function
    i = 1
    d = 2.0
    s = 'Hello'
    mylib.__my_module_MOD_my_subroutine(
            ct.byref(ct.c_int64(i)),
            ct.byref(ct.c_double(d)),
            ct.c_char_p(s),
            ct.c_int(len(s)))

**Important:**
    * The mangeled symbol name has to be used

    * Fortran passes by reference. Get the reference to non-pointer types by calling
      the ctypes function ``byref``

    * The length of the assumed length character gets passed **by value** at the end of
      the function call

.. code-block:: none
    :caption: Run this example:

    $ python run.py
     i =                     1
     d =    2.0000000000000000     
     s = Hello

Use C ABI with bind(C)
----------------------

Include the ``iso_c_binding`` module and change the source file:
    * ``bind(C)`` keyword after subroutine signature
    * Only use C type arguments (i.e. C style string instead of Fortran assumed length character)

.. code-block:: fortran
    :caption: fcode.f90

    module my_module
    use, intrinsic :: iso_fortran_env
    use :: iso_c_binding
    implicit none

    contains

    subroutine my_subroutine(i, d, s) bind(C)

    integer(c_int64_t), intent(in) :: i
    real(c_double), intent(in) :: d
    character(c_char), dimension(*), intent(in) :: s

    integer :: s_len = 0
    do
        if (s(s_len+1) == c_null_char) exit
        s_len = s_len + 1
    end do

    write(*,*) "i = ", i
    write(*,*) "d = ", d
    write(*,*) "s = ", s(:s_len)

    end subroutine

    end module

You have to do the conversion between C strings (``\0`` terminated) and Fortran assumed length
character (length passed as hidden argument) yourself!

.. code-block:: none
    :caption: On the bright side, the Fortran compiler produced a C ABI conform binary:
    :emphasize-lines: 4

    $ nm libmylib.so
    ...
    0000000000000800 t register_tm_clones
    00000000000008c0 T my_subroutine
    0000000000201050 d __TMC_END__
    ...

Call the subroutine like in the C case (but arguments are still passed by reference!):

.. code-block:: python
    :caption: run.py

    import numpy as np
    import ctypes as ct
    import os

    # Load library
    libname = 'libmylib'
    libpath = os.path.join(os.path.dirname(__file__), '.')
    mylib = np.ctypeslib.load_library(libname, libpath)

    # Call function
    i = 1
    d = 2.0
    s = 'Hello'
    mylib.my_subroutine(
            ct.byref(ct.c_int64(i)),
            ct.byref(ct.c_double(d)),
            ct.c_char_p(s))

.. code-block:: none
    :caption: Test:

    $ python run.py
     i =                     1
     d =    2.0000000000000000     
     s = Hello
