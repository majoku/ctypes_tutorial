First example
=============

Let's start with a simple C function:

.. code-block:: c
    :caption: ccode.c

    #include <stdlib.h>
    #include <stdio.h>

    void my_function(int64_t i, double d, char *s)
    {
        printf("i = %ld\n", i);
        printf("d = %f\n", d);
        printf("s = %s\n", s);
    }

Important for compiling:
    * ``-fPIC`` (position independent code)
Important for linking:
    * ``-shared`` (produce shared object)

.. code-block:: none

    gcc -c -fPIC ccode.c
    gcc -shared -Wl,-soname,libmylib.so ccode.o -o libmylib.so

This produces the ``ccode.o`` object file and ``libmylib.so`` shared object.
You can check the names of defined symbols in binary files with the namelist (``nm``) command:

.. code-block:: none
    :emphasize-lines: 4

    $ nm libmylib.so
    ...
    0000000000000610 t register_tm_clones
    00000000000006d0 T my_function
    0000000000201028 d __TMC_END__
    ...

**Important**: C++ and Fortran compilers mangle the names:

.. code-block:: none

    g++ -c -fPIC ccode.c
    g++ -shared -Wl,-soname,libmylib.so ccode.o -o libmylib.so

.. code-block:: none
    :emphasize-lines: 4

    $ nm libmylib.so
    ...
    00000000002009b0 d __TMC_END__
    0000000000000650 T _Z13my_functionldPc

More on that in the Fortran example.

Now let's have a look at the Python file:

.. code-block:: python
    :caption: run.py

    import numpy as np
    import ctypes as ct
    import os

    # Load library
    libname = 'libmylib'
    libpath = os.path.join(os.path.dirname(__file__), '.')
    mylib = np.ctypeslib.load_library(libname, libpath)

    # Call function
    i = 1
    d = 2.0
    s = 'Hello'
    mylib.my_function(
            ct.c_int64(i),
            ct.c_double(d),
            ct.c_char_p(s))

We first load the library using NumPy's ``ctypeslib.load_library`` function, which
is a wrapper for ctypes' ``cdll`` function. It takes the library's name
(With 'lib' prefix, but without file extension) and location.

The C function ``my_function`` becomes a method of the library object. All arguments
need to be converted to ctypes types.

Example of ctypes types (more in documentation https://docs.python.org/2/library/ctypes.html):

===========     =======
ctypes type     C type
===========     =======
c_bool          _Bool
c_int           int
c_int32         int32_t
c_int64         int64_t
c_float         float
c_double        double
c_char_p        char*
===========     =======

.. code-block:: none
    :caption: Let's now run our example:

    $ python run.py
    i = 1
    d = 2.000000
    s = Hello
