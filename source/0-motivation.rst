Motivation
==========

    * Python allows us to write easy to read, high level code
      for efficient prototyping

    * Object-oriented Python code increases readability and maintanability

    * Python code is flexible due to duck typing

    * Python has a great scientific ecosystem and user base

    **BUT**

    * Available Python interpreters are slow (function calls, loops)

    * No shared memory parellization in pure Python

    * If you loop in Python, you lose!

    **Solution:**

    Write hot code in low level language (C/C++, Fortran) and interface it via:

        * Python extension module
        * **ctypes**
        * cffi
        * SWIG
        * f2py
        * Cython
        * ...
