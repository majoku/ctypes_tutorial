.. ctypes_tutorial documentation master file, created by
   sphinx-quickstart on Tue Apr  3 16:05:40 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Introduction to ctypes
======================

    * Clone repository:
        ``git clone https://gitlab.com/majoku/ctypes_tutorial.git``
    * Build documentation (location: build/html/index.html):
        ``cd ctypes_tutorial && make html``
    * **Have a look at the examples**

    * Useful links:
        * ctypes documentation: https://docs.python.org/2/library/ctypes.html
        * SciPy cookbook: http://scipy-cookbook.readthedocs.io/items/Ctypes.html
        * mpi4py: http://mpi4py.scipy.org/docs/

.. toctree::
    :maxdepth: 2
    :numbered:
    :caption: Contents:

    0-motivation
    1-first-example
    2-first-example-fortran
    3-argtypes
    4-numpy
    5-structs
    6-null-pointers
    7-callbacks
    8-openmp
    9-mpi
