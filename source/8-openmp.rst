OpenMP
======

CPython has a global interpreter lock (GIL). Due to the GIL, shared memory parallelization (SMP)
cannot speed up pure Python code. However, when we call C using ctypes, the GIL gets released and
the C code benefits from SMP!

.. code-block:: c
    :caption: ccode.c
    :emphasize-lines: 10-12

    #include <stdlib.h>
    #include <stdio.h>
    #include <assert.h>

    double my_sum(size_t size, double array[])
    {
        assert(size >= 0 && array);

        double sum = 0.0;
    #ifdef _OPENMP
    #pragma omp parallel for reduction(+:sum)
    #endif
        for (size_t idx = 0; idx < size; idx++) {
            sum += array[idx];
        }
        return sum;
    }
