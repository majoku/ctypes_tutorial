Structures
==========

Let's introduce a simple struct which holds array data and the array's size:

.. code-block:: c
    :caption: ccode.c
    :emphasize-lines: 4-7

    #include <stdlib.h>
    #include <stdio.h>

    struct my_array {
        size_t size;
        double *data;
    };

    int my_function(struct my_array *a)
    {
        if (!a) return 1;
        if (!a->data) return 2;

        for (size_t i = 0; i < a->size; i++) {
            printf("a[%ld] = %f\n", i, a->data[i]);
        }

        return 0;
    }

We can define this struct in Python by inheriting from ``ctypes.Structure``
and setting the ``_fields_`` atttribute. ``_fields_`` must be a sequence of 2-tuples,
containing the field's name and type:

.. code-block:: python
    :caption: run.py
    :emphasize-lines: 11-15, 25

    import numpy as np

	import ctypes as ct
	import os

	# Load library
	libname = 'libmylib'
	libpath = os.path.join(os.path.dirname(__file__), '.')
	mylib = np.ctypeslib.load_library(libname, libpath)

	# Define my_array struct
	class my_array(ct.Structure):
	    _fields_ = (
	            ('size', ct.c_size_t),
	            ('data', np.ctypeslib.ndpointer(ndim=1, dtype=np.double, flags='A,C')))

	# Define interface
	mylib.my_function.argtypes = [
	    ct.POINTER(my_array)
	]
	mylib.my_function.restype = ct.c_int

	# Wrapper
	def my_function(a):
	    struct = my_array(len(a), a.ctypes.data)
	    ierr = mylib.my_function(struct)
	    if ierr: raise RuntimeError('Error in libmylib.my_function')

	# Call wrapper
	a = np.arange(6.0)
	my_function(a)
