ctypes and NumPy arrays
=======================

NumPy's ctypeslib provides some more helper function for use with with ctypes.

A simple example that prints every element of a double vector:

.. code-block:: c
    :caption: ccode.c

    #include <stdlib.h>
    #include <stdio.h>

    int my_function(size_t n, double a[])
    {
        if (!a) return 1;

        for (size_t i = 0; i < n; i++) {
            printf("a[%ld] = %f\n", i, a[i]);
        }

        return 0;
    }


NumPy's ``ctypeslib.ndpointer`` is a factory function that can be used in ``argtypes`` and ``restype``
specification. Several restrictions can be specified via the arguments ``dtype``, ``ndim``,
``shape``, and ``flags``. If a given array does not satisfy the restrictions, NumPy will raise
a ``TypeException``.

.. code-block:: python
    :caption: run.py
    :emphasize-lines: 13

    import numpy as np
    import ctypes as ct
    import os

    # Load library
    libname = 'libmylib.so'
    libpath = os.path.join(os.path.dirname(__file__), '.')
    mylib = np.ctypeslib.load_library(libname, libpath)

    # Define interface
    mylib.my_function.argtypes = [
            ct.c_size_t,
            np.ctypeslib.ndpointer(ndim=1, dtype=np.double)
    ]
    mylib.my_function.restype = ct.c_int

    # Call function
    a = np.arange(6.0)
    ierr = mylib.my_function(len(a), a)
    if ierr: raise RuntimeError('Error in libmylib.my_function')

.. code-block:: none
    :caption: Output

    $ python run.py
    a[0] = 0.000000
    a[1] = 1.000000
    a[2] = 2.000000
    a[3] = 3.000000
    a[4] = 4.000000
    a[5] = 5.000000

Wrappers for library calls
--------------------------

Thin wrappers should handle simple tasks (i.e. Determining data types and array sizes) and
handling of return codes:

.. code-block:: python
    :caption: run.py
    :emphasize-lines: 17-20

    import numpy as np
    import ctypes as ct
    import os

    # Load library
    libname = 'libmylib'
    libpath = os.path.join(os.path.dirname(__file__), '.')
    mylib = np.ctypeslib.load_library(libname, libpath)

    # Define interface
    mylib.my_function.argtypes = [
            ct.c_size_t,
            np.ctypeslib.ndpointer(ndim=1, dtype=np.double)
    ]
    mylib.my_function.restype = ct.c_int

    # Wrapper
    def my_function(a):
        ierr = mylib.my_function(len(a), a)
        if ierr: raise RuntimeError('Error in libmylib.my_function')

    # Call wrapper
    a = np.arange(6.0)
    my_function(a)

The function call is now much more pythonic.

Check array flags
-----------------

What happens if we call the above wrapper with ``a[::2]`` as argument?

.. code-block:: python
    :caption: run.py
    :emphasize-lines: 24

    import numpy as np
    import ctypes as ct
    import os

    # Load library
    libname = 'libmylib'
    libpath = os.path.join(os.path.dirname(__file__), '.')
    mylib = np.ctypeslib.load_library(libname, libpath)

    # Define interface
    mylib.my_function.argtypes = [
            ct.c_size_t,
            np.ctypeslib.ndpointer(ndim=1, dtype=np.double)
    ]
    mylib.my_function.restype = ct.c_int

    # Wrapper
    def my_function(a):
        ierr = mylib.my_function(len(a), a)
        if ierr: raise RuntimeError('Error in libmylib.my_function')

    # Call wrapper
    a = np.arange(6.0)
    my_function(a[::2])


.. code-block:: none
    :caption: Output

    $ python run.py
    a[0] = 0.000000
    a[1] = 1.000000
    a[2] = 2.000000

What went wrong? NumPy **basic indexing** returns a **view** into the original data, i.e.
no data gets copied! C thus receives the pointer to the original array.

How do we make sure that this does not happen? We have to inspect the ``flag`` attribute:

.. code-block:: none
    :caption: a.flags
    :emphasize-lines: 1-3

      C_CONTIGUOUS : True
      F_CONTIGUOUS : True
      OWNDATA : True
      WRITEABLE : True
      ALIGNED : True
      UPDATEIFCOPY : False

.. code-block:: none
    :caption: a[::2].flags
    :emphasize-lines: 1-3

      C_CONTIGUOUS : False
      F_CONTIGUOUS : False
      OWNDATA : False
      WRITEABLE : True
      ALIGNED : True
      UPDATEIFCOPY : False

If we require the given array to have the flags ``ALIGNED`` and ``C_CONTIGUOUS``,
the function will no longer work with non-contiguous data like ``a[::2]``:

.. code-block:: python
    :caption: run.py
    :emphasize-lines: 13

    import numpy as np
    import ctypes as ct
    import os

    # Load library
    libname = 'libmylib'
    libpath = os.path.join(os.path.dirname(__file__), '.')
    mylib = np.ctypeslib.load_library(libname, libpath)

    # Define interface
    mylib.my_function.argtypes = [
            ct.c_size_t,
            np.ctypeslib.ndpointer(ndim=1, dtype=np.double, flags='A,C')
    ]
    mylib.my_function.restype = ct.c_int

    # Wrapper
    def my_function(a):
        ierr = mylib.my_function(len(a), a)
        if ierr: raise RuntimeError('Error in libmylib.my_function')

    # Call wrapper
    a = np.arange(6.0)
    my_function(a[::2])

.. code-block:: none
	:caption: Output

	Traceback (most recent call last):
	  File "run.py", line 24, in <module>
	    my_function(a[::2])
	  File "run.py", line 19, in my_function
	    ierr = mylib.my_function(len(a), a)
	ctypes.ArgumentError: argument 2: <type 'exceptions.TypeError'>: array must have flags ['C_CONTIGUOUS', 'ALIGNED']


Always set all required flags in the ``ndpointer`` call.
In particular, if you write to an array, check that it is writeable!
