Callbacks
=========

Sometimes it can be useful to call back to Python (from C).
Consider the following function:

.. code-block:: c
    :caption: ccode.c

    #include <stdlib.h>
    #include <stdio.h>

    int my_function(size_t n, double x[], double (*func)(double))
    {
        if (!func) return 1;

        for (size_t i = 0; i < n; i++) {
            printf("func(%f) = %f\n", x[i], func(x[i]));
        }

        return 0;
    }

We can use ``ctypes.CFUNCTYPE`` to create a factory for C functions:

.. code-block:: python
    :caption: run.py
    :emphasize-lines: 10-11, 17, 23

    import numpy as np
    import ctypes as ct
    import os

    # Load library
    libname = 'libmylib'
    libpath = os.path.join(os.path.dirname(__file__), '.')
    mylib = np.ctypeslib.load_library(libname, libpath)

    # Define C function type
    cfunc_t = ct.CFUNCTYPE(ct.c_double, ct.c_double)

    # Define interface
    mylib.my_function.argtypes = [
            ct.c_size_t,
            np.ctypeslib.ndpointer(ndim=1, dtype=np.double, flags='A,C'),
            cfunc_t
    ]
    mylib.my_function.restype = ct.c_int

    # Wrapper
    def my_function(x, func):
        cfunc = cfunc_t(func)
        ierr = mylib.my_function(len(x), x, cfunc)
        if ierr: raise RuntimeError('Error in libmylib.my_function')

    # Call wrapper
    x = np.arange(10.0)
    f = lambda x : x**2.0
    my_function(x, f)

.. code-block:: none
    :caption: Output

    func(0.000000) = 0.000000
    func(1.000000) = 1.000000
    func(2.000000) = 4.000000
    func(3.000000) = 9.000000
    func(4.000000) = 16.000000
    func(5.000000) = 25.000000
    func(6.000000) = 36.000000
    func(7.000000) = 49.000000
    func(8.000000) = 64.000000
    func(9.000000) = 81.000000

Remember that this will slow down you code considerably, so **no callbacks in hot code**.

Another common use for callbacks is memory allocation
(see also http://scipy-cookbook.readthedocs.io/items/Ctypes.html):

.. code-block:: python
    :caption: run.py
    :emphasize-lines: 40, 46-47, 53

    import numpy as np
    import ctypes as ct
    import os

    # Load library
    libname = 'libmylib'
    libpath = os.path.join(os.path.dirname(__file__), '.')
    mylib = np.ctypeslib.load_library(libname, libpath)

    # Define Allocator class
    class Allocator(object):
        """Allocator class to callback from C to Python for memory allocation.

        Use:
            foo.argtypes = [..., Allocator.CFUNCTYPE,...]
            alloc = Allocator()
            foo(..., alloc.cfunc,...)
            result = alloc.value
        """

        CFUNCTYPE = ct.CFUNCTYPE(ct.c_void_p, ct.c_size_t, ct.POINTER(ct.c_size_t), ct.c_char)

        def __init__(self):
            self.value = None

        def __call__(self, ndim, shape, dtype):
            try:
                self.value = np.zeros(shape=shape[:ndim], dtype=np.dtype(dtype))
                return self.value.ctypes.data
            except Exception as e:
                print(e)
                return None

        @property
        def cfunc(self):
            return self.CFUNCTYPE(self)

    # Define interface
    mylib.my_function.argtypes = [
            Allocator.CFUNCTYPE
    ]
    mylib.my_function.restype = ct.c_int

    # Wrapper
    def my_function():
        alloc = Allocator()
        ierr = mylib.my_function(alloc.cfunc)
        if ierr: raise RuntimeError('Error in libmylib.my_function')
        return alloc.value

    # Call wrapper
    array = my_function()
    print(array)

.. code-block:: c
    :caption: ccode.c

    #include <stdlib.h>
    #include <stdio.h>

    typedef void* (*allocator_t)(size_t, size_t[], char);

    int my_function(allocator_t alloc)
    {
        /* Allocate a 2D double array with shape (3, 4) */
        size_t ndim = 2;
        size_t shape[] = {3, 4};
        char dtype = 'd';
        double *array = (double *) alloc(ndim, shape, dtype);

        /* Fill array with values */
        for (size_t i = 0; i < shape[0]; i++) {
            for (size_t j = 0; j < shape[1]; j++) {
                array[i*shape[1]+j] = i*shape[1]+j;
            }
        }

        return 0;
    }
