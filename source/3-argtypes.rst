Using ``argtypes`` and ``restype``
==================================

So far, we are calling on the binary level without any checks - **this is dangerous!**

.. code-block:: python
    :emphasize-lines: 3
    :caption: Example of a wrong call:

    mylib.my_function(
            ct.c_int64(i),
            ct.c_int64(j),
            ct.c_double(d),
            ct.c_char_p(s))

.. code-block:: none
    :emphasize-lines: 3
    :caption: Output

    i = 1
    d = 2.000000e+00
    Segmentation fault (core dumped)


.. code-block:: python
    :emphasize-lines: 2
    :caption: Even worse, a wrong call can go unnoticed:

    mylib.my_function(
            ct.c_double(i),
            ct.c_double(d),
            ct.c_char_p(s))

.. code-block:: none
    :caption: Output

	i = 140357070821380
	d = 1.000000e+00
	s = 

To mitigate these issues, we can let ctypes know what the signature of the function is and it
will raise an exception if we try to call the function incorrectly.

This is done with the ``argtypes`` and ``restype`` attributes.

First example with small modification:

.. code-block:: c
    :caption: ccode.c

    #include <stdlib.h>
    #include <stdio.h>

    double my_function(int64_t i, double d, char *s)
    {
        printf("i = %ld\n", i);
        printf("d = %f\n", d);
        printf("s = %s\n", s);

        return i + d;
    }

The ``argtypes`` attribute must be a sequence of C types. ``restype`` must be a single C type.

.. code-block:: python
    :caption: run.py
    :emphasize-lines: 10-16,22,23

    import numpy as np
    import ctypes as ct
    import os

    # Load library
    libname = 'libmylib'
    libpath = os.path.join(os.path.dirname(__file__), '.')
    mylib = np.ctypeslib.load_library(libname, libpath)

    # Define interface
    mylib.my_function.argtypes = [
            ct.c_int64,
            ct.c_double,
            ct.c_char_p
    ]
    mylib.my_function.restype = ct.c_double

    # Call function
    i = 1
    d = 2.0
    s = 'Hello'
    result = mylib.my_function(i, d, s)
    print('result = %f' % result)

As a bonus, we do no longer need to convert the arguments to ctypes types - since ctypes
knows the types, it can do it for us (only if it makes sense,
i.e. it will not convert a Python double to a C integer)

.. code-block:: none
    :caption: Run code like before:

    $ python run.py
    i = 1
    d = 2.000000
    s = Hello
    result = 3.000000

Fortran
-------

Changes to  the source file:

.. code-block:: fortran
    :caption: fcode.f90
    :emphasize-lines: 7, 12, 17

    module my_module
    use, intrinsic :: iso_fortran_env
    implicit none

    contains

    subroutine my_subroutine(i, d, s, r)

    integer(INT64), intent(in) :: i
    real(REAL64), intent(in) :: d
    character(len=*), intent(in) :: s
    real(REAL64), intent(out) :: r

    write(*,*) "i = ", i
    write(*,*) "d = ", d
    write(*,*) "s = ", s
    r = i + d

    end subroutine

    end module

Create a pointer type (if not already available, like ``c_char_p``) by calling
the ctypes factory function ``POINTER`` with the target C type as argument:

.. code-block:: python
    :caption: run.py
    :emphasize-lines: 10-18,24-31

    import numpy as np
    import ctypes as ct
    import os

    # Load library
    libname = 'libmylib'
    libpath = os.path.join(os.path.dirname(__file__), '.')
    mylib = np.ctypeslib.load_library(libname, libpath)

    # Define interface
    mylib.__my_module_MOD_my_subroutine.argtypes = [
            ct.POINTER(ct.c_int64),
            ct.POINTER(ct.c_double),
            ct.c_char_p,
            ct.POINTER(ct.c_double),
            ct.c_int
    ]
    mylib.__my_module_MOD_my_subroutine.restype = None

    # Call function
    i = 1
    d = 2.0
    s = 'Hello'
    result = ct.c_double()
    mylib.__my_module_MOD_my_subroutine(
            ct.byref(ct.c_int64(i)),
            ct.byref(ct.c_double(d)),
            s,
            ct.byref(ct.c_double(result)),
            len(s))
    print('result = %f' % result.value)

.. code-block:: none
    :caption: Result:

    $ python run.py
     i =                     1
     d =    2.0000000000000000     
     s = Hello
    result = 3.000000
