Prerequisites
=============

* Python 2.7
* Sphinx
* gcc (including gfortran)

Build HTML
==========

* make html
* firefox build/html/index.html
